How to run this project
----

```
git clone git@bitbucket.org:hyfather/statsmaker.git

cd statsmaker/config
ln -s database.yml.example database.yml #or create a new database.yml
# MANUALLY create a new MySQL user with DDL privileges
cd ../

bundle install
rake db:create db:migrate
rake test #all tests should ideally pass

#To run the development server:
rake db:seed #can be invoked with dataset_size=(small|medium|large)
rails server
```