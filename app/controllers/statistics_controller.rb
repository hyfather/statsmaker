class StatisticsController < ApplicationController

  def top_urls
    top_urls = Visit.top_urls_for_the_last_5_days
    respond_to do |format|
      format.json { render :json => top_urls }
      format.html { @payload = top_urls; render :pretty_json }
    end
  end

  def top_referrers
    top_urls_with_referrers = Visit.top_urls_with_referrers_for_the_last_5_days
    respond_to do |format|
      format.json { render :json => top_urls_with_referrers }
      format.html { @payload = top_urls_with_referrers; render :pretty_json }
    end
  end
end
