class Visit < Sequel::Model

  # Two database writes take place when creating one record. This is less
  # than ideal but is the most simple way to reliably include the auto_id
  # in the integrity_hash.
  # Double-writes on this model take 33% more time than single writes when
  # tested against 100,000 sequencial record creations.

  # For a more performant single-operation requirement, the auto_id
  # would have to be generated in the application code or the 
  # integrity_hash would have to be calculated on the database.

  def around_create
    self.integrity_hash = "invalid"
    yield
    self.integrity_hash = calculate_integrity_hash
    self.save
  end

  def self.top_urls_for_the_last_5_days
    top_urls_between(5.days.ago, Time.now)
  end

  def self.top_urls_with_referrers_for_the_last_5_days
    top_urls_with_referrers_between(5.days.ago, Time.now)
  end


  # Note: 'from time' is chronologically earlier than 'to time'
  def self.top_urls_between(from, to)
    objs = top_urls_dataset(from, to).all

    hsh = {}
    objs.each do |visit|
      day = visit[:created_at].strftime("%Y-%m-%d")
      hsh[day] ||= []
      hsh[day] << {"url" => visit[:url], "visits" => visit[:visits]}
    end
    hsh
  end

  def self.top_urls_with_referrers_between(from, to)
    objs = top_refs_dataset(from, to).all

    hsh = {}
    objs.each do |visit|
      day = visit[:created_at].strftime("%Y-%m-%d")
      hsh[day] ||= []

      ref_entry = {"url" => visit[:referrer], "visits" => visit[:visitcount]}
      url_entry = hsh[day].find{|v| v["url"] == visit[:url]}

      if url_entry
        url_entry["referrers"] << ref_entry
      else
        hsh[day] << {"url" => visit[:url], "referrers" => [ref_entry]}
      end
    end
    
    hsh = rollup_total_visits(hsh)
    hsh = limit_entries(hsh)
    hsh
  end

  private

  def self.top_urls_dataset(from, to)
    indexed_group_dataset = self.select(:url, :created_at).
      select_append{Sequel.as(count(1), :visits)}.
      filter{created_at > from}.
      filter{created_at < to}.
      group(:url, :created_at)

    casted_date = Sequel.cast(:created_at, :date)
    self.select(:url).
      select_append{Sequel.as(casted_date, :created_at)}.
      select_append(:visits).
      from(indexed_group_dataset).
      group(:url, casted_date).
      order(Sequel.desc(:visits))
  end

  def self.top_refs_dataset(from, to)
    indexed_group_dataset = self.select(:id, :url, :created_at, :referrer).
      select_append{Sequel.as(count(1), :refcount)}.
      filter{created_at > from}.
      filter{created_at < to}.
      group(:url, :created_at, :referrer)

    # grouping by casted date doesn't benefit from indexes, hence
    # we group by a casted date on a smaller, already grouped ds.
    casted_date = Sequel.cast(:created_at, :date)
    self.select(:url, :referrer).
      select_append{Sequel.as(casted_date, :created_at)}.
      from{indexed_group_dataset}.
      select_append{Sequel.as(sum(:refcount), :visitcount)}.
      group(:url, casted_date, :referrer)
  end

  def self.rollup_total_visits(visits_hsh)
    visits_hsh.each do |day, daily_visits|
      daily_visits.each do |url|
        daily_total = url["referrers"].map{|r| r["visits"]}.reduce(:+)
        url["visits"] = daily_total
      end
    end
    visits_hsh
  end

  def self.limit_entries(visits_hsh, params={})
    limits = {:urls => 10, :refs => 5}.merge(params)
    visits_hsh.each do |day, daily_visits|
      daily_visits.sort! {|a, b| b["visits"] <=> a["visits"]}
      daily_visits = daily_visits.first(limits[:urls])
      daily_visits.each do |visit|
        visit["referrers"].sort! {|a, b| b["visits"] <=> a["visits"]}
        visit["referrers"] = visit["referrers"].first(limits[:refs])
      end
    end
    visits_hsh
  end

  def calculate_integrity_hash
    string_representation = {:id => id,
      :url => url,
      :referrer => referrer,
      :created_at => created_at
    }.to_s
    Digest::MD5.hexdigest(string_representation)
  end
end
