Sequel.migration do 
  change do

    # Choosing to name the `hash` field as `integrity_hash` to avoid name
    # collision with Ruby's Object#hash. The column could have retained
    # the `hash` name and the model could have aliased it to something else
    # but that has the potential to cause unforeseen bugs while maintaining
    # the codebase. The aliasing approach is more suitable while working
    # with a legacy database schema that is hard to change.

    create_table :visits do
      primary_key :id
      String :url, :null => false
      String :referrer
      Time :created_at, :null => false
      String :integrity_hash, :null => false
    end

  end
end
