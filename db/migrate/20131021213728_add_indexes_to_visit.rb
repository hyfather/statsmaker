Sequel.migration do
  change do
    alter_table :visits do
      add_index :created_at
      add_index [:url, :created_at, :referrer]
    end
  end
end
