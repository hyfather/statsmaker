Sequel.migration do
  change do
    create_table(:schema_migrations) do
      column :filename, "varchar(255)", :null=>false
      
      primary_key [:filename]
    end
    
    create_table(:visits) do
      primary_key :id, :type=>"int(11)"
      column :url, "varchar(255)", :null=>false
      column :referrer, "varchar(255)"
      column :created_at, "datetime", :null=>false
      column :integrity_hash, "varchar(255)", :null=>false
      
      index [:created_at], :name=>:created_at
      index [:referrer], :name=>:referrer
      index [:url, :created_at, :referrer], :name=>:url
      index [:created_at]
      index [:url, :created_at, :referrer]
    end
  end
end
Sequel.migration do
  change do
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20131017195004_create_visits.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20131021213728_add_indexes_to_visit.rb')"
  end
end
