logger = Logger.new(STDOUT)
logger.info("Invocation: `rake db:seed dataset_size=SIZE`.")
logger.info("SIZE can be 'small', 'medium' or 'large' to create 100, 10_000 or 1_000_000 records.")

dataset_sizes = {
  "small" => 100,
  "medium" => 10_000,
  "large" => 1_000_000 }
dataset_sizes.default = 100
total_records = dataset_sizes[ENV['dataset_size'].to_s.downcase]

logger.info("Will create #{total_records} records")

seed_urls = [
        "http://apple.com",
        "https://apple.com",
        "https://www.apple.com",
        "http://developer.apple.com",
        "http://en.wikipedia.org",
        "http://opensource.org",
        "http://news.ycombinator.com",
        "http://www.reddit.com",
        "http://gmane.org",
        "https://lkml.org"
       ]

seed_referrers = [
             "http://apple.com",
             "https://apple.com",
             "https://www.apple.com",
             "http://developer.apple.com",
             "http://www.google.com",
             "http://www.yahoo.com",
             "http://www.bing.com",
             nil, nil
            ]

seed_ctimes = (1..10).to_a.map{|n| n.days.ago + (1..24).to_a.sample.hours }

total_records.times do
  visit = Visit.new(:url => seed_urls.sample,
                    :referrer => seed_referrers.sample,
                    :created_at => seed_ctimes.sample)
  visit.save
  logger.info "Created record ##{visit.id}" if (visit.id % 1000) == 0
end

logger.info "Total records created: #{total_records}"
