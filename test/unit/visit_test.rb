require 'test_helper'
require "minitest/autorun"

class VisitTest < ActiveSupport::TestCase
  def setup
    Visit.select("*").delete
  end

  def test_that_an_integrity_hash_is_set_upon_creation
    time_now = Time.now
    visit = Visit.create(:url => "http://apple.com",
                         :referrer => "http://google.com",
                         :created_at => time_now)
    md5 = Digest::MD5.hexdigest({:id => visit.id,
                                  :url => "http://apple.com",
                                  :referrer => "http://google.com",
                                  :created_at => time_now
                                }.to_s)

    assert_equal md5, visit.integrity_hash
  end

  def test_that_once_updated_the_integrity_hash_will_be_invalid
    time_now = Time.now
    visit = Visit.create(:url => "http://apple.com",
                         :referrer => "http://google.com",
                         :created_at => time_now)
    
    visit.referrer = "http://wikipedia.com"
    visit.save
    visit.reload
    
    md5 = Digest::MD5.hexdigest({:id => visit.id,
                                  :url => "http://apple.com",
                                  :referrer => "http://wikipedia.com",
                                  :created_at => time_now
                                }.to_s)
    assert_not_equal md5, visit.integrity_hash
  end

  def test_that_integrity_hash_is_always_automatically_generated
    visit = Visit.create(:url => "http://apple.com",
                         :referrer => "http://google.com",
                         :created_at => Time.now,
                         :integrity_hash => "abcd")
    assert_not_equal "abcd", visit.integrity_hash
  end


  def test_that_an_aggregated_count_of_visits_by_date_can_be_retrieved
    jan1_a = Time.at(1)
    jan1_b = jan1_a + 1.hour
    jan2 = jan1_a + 1.day

    visit1 = Visit.create(:url => "http://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan1_a)

    visit1a = Visit.create(:url => "http://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan1_a)

    visit2 = Visit.create(:url => "https://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan1_b)

    visit3 = Visit.create(:url => "http://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan2)

    top_urls = Visit.top_urls_between(Time.at(0), Time.at(0) + 5.days)

    expected_stats = {
      "1969-12-31" => [
                       {"url" => "http://apple.com", "visits" => 2},
                       {"url" => "https://apple.com", "visits" => 1}
                      ],
      "1970-01-01" => [
                       {"url" => "http://apple.com", "visits" => 1}
                      ]
    }

    assert_equal expected_stats, top_urls
  end

  def test_that_an_invalid_timebound_fails_with_empty_result
    assert_equal({}, Visit.top_urls_between(Time.at(10), Time.at(5)))
  end

  def test_that_top_urls_with_referrers_can_be_retrieved
    jan1_a = Time.at(1)
    jan1_b = jan1_a + 1.hour
    jan2 = jan1_a + 1.day

    visit1 = Visit.create(:url => "http://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan1_a)

    visit1a = Visit.create(:url => "http://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan1_a)

    visit2 = Visit.create(:url => "http://apple.com",
                          :referrer => "http://wikipedia.org",
                          :created_at => jan1_b)

    visit3 = Visit.create(:url => "http://apple.com",
                          :referrer => "http://google.com",
                          :created_at => jan2)

    top_urls_with_referrers = Visit.top_urls_with_referrers_between(Time.at(0), Time.at(0) + 5.days)


    expected_stats = {
      "1969-12-31" => [
                       {
                         "url" => "http://apple.com", "visits" => 3,
                         "referrers" => [
                                         {"url" => "http://google.com", "visits" => 2},
                                         {"url" => "http://wikipedia.org", "visits" => 1}
                                        ]
                       },
                      ],
      "1970-01-01" => [
                       {"url" => "http://apple.com", "visits" => 1,
                         "referrers" => [
                                         {"url" => "http://google.com", "visits" => 1}
                                        ]
                       }
                      ]
    }
    
    assert_equal expected_stats, top_urls_with_referrers
  end

  def test_that_an_invalid_timebound_for_referrers_fails_with_empty_result
    assert_equal({}, Visit.top_urls_with_referrers_between(Time.at(10), Time.at(5)))
  end

  def xtest_that_limits_of_10_and_5_are_imposed_on_referrer_queries
    raise "not implemented yet"
  end
end
